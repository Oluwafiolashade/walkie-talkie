/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#define OFF 0x01
#define ON 0x00
#define OFF_COM 0x00
#define ON_COM 0x01


uint8 sizes,values;
uint8 LCD_setDisplay,LCD_ClearDisplay;
void display_number(uint8 position, uint8 values)
                {
                    if(position == 0)
                    {
                        Pin_14_Write(ON_COM);
                        Pin_15_Write(OFF_COM);
                         switch(values)
                        {
                            case 0:             //To display 0
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(OFF);
                                break;
                            }
                            case 1:             //To display 1
                            {
                                Pin_2_Write(OFF);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(OFF);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(OFF);
                                break;
                            }
                             case 2:            //To display 2
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(OFF);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 3:             //To display 3
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                             case 4:        //To display 4
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 5:         //To display 5
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(OFF);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                             case 6:        //To display 6
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(OFF);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 7:        //To display 7
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(OFF);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(OFF);
                                break;
                            }
                             case 8:        //To display 8
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 9:         //To display 9
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break; 
                            }
                            case 10:        //To display dot
                            {
                               Pin_1_Write(ON);
                               break;
                            }
                            case 11:        //To display off dot
                            {
                               Pin_1_Write(OFF);
                               break;
                            }
                             case 12:        //To off all
                            {
                               Pin_1_Write(OFF);
                               Pin_2_Write(OFF);
                               Pin_3_Write(OFF);
                               Pin_4_Write(OFF);
                               Pin_5_Write(OFF);
                               Pin_6_Write(OFF);
                               Pin_7_Write(OFF);
                               Pin_8_Write(OFF);
                               break;
                            }
                        }
                            
                    }
                     if(position == 1)
                    {
                         Pin_14_Write(OFF_COM);
                         Pin_15_Write(ON_COM);
                        switch(values)
                        {
                            case 0:        //To display 0
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(OFF);
                                break;
                            }
                            case 1:         //To display 1
                            {
                                Pin_2_Write(OFF);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(OFF);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(OFF);
                                break;
                            }
                             case 2:        //To display 2
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(OFF);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 3:         //To display 3
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                             case 4:            //To display 4
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 5:         //To display 5
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(OFF);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(OFF);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                             case 6:        //To display 6
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(OFF);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 7:         //To display 7
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(OFF);
                                Pin_6_Write(OFF);
                                Pin_7_Write(OFF);
                                Pin_8_Write(OFF);
                                break;
                            }
                             case 8:        //To display 8
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(ON);
                                Pin_8_Write(ON);
                                break;
                            }
                            case 9:         //To display 9
                            {
                                Pin_2_Write(ON);
                                Pin_3_Write(ON);
                                Pin_4_Write(ON);
                                Pin_5_Write(ON);
                                Pin_6_Write(ON);
                                Pin_7_Write(OFF);
                                Pin_8_Write(ON);
                                break; 
                            }
                            case 10:        //To display dot
                            {
                               Pin_1_Write(ON);
                               break;
                            }
                            case 11:        //To off dot
                            {
                               Pin_1_Write(OFF);
                               break;
                            }
                             case 12:        //To off all
                            {
                               Pin_1_Write(OFF);
                               Pin_2_Write(OFF);
                               Pin_3_Write(OFF);
                               Pin_4_Write(OFF);
                               Pin_5_Write(OFF);
                               Pin_6_Write(OFF);
                               Pin_7_Write(OFF);
                               Pin_8_Write(OFF);
                               break;
                            }
                        }
                        
                    }
    return; 
}
            
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    //start lcd operations and output display 

    for(;;)
    {
     CyDelay(1);
     display_number(0,0);
     CyDelay(1);
     display_number(1,0);
             
    }
}

/* [] END OF FILE */
